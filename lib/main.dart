import 'package:flutter/material.dart';
import 'package:platform_app/views/About/About.dart';
import 'package:platform_app/views/Error/Error.dart';
import 'package:platform_app/views/Home/Home.dart';
import 'package:platform_app/utils/configNonWeb.dart'
    if (dart.library.html) 'package:platform_app/utils/configWeb.dart';
import 'package:platform_app/views/NotFound/NotFound.dart';

void main() {
  configureApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/app',
      routes: {
        '/app': (BuildContext context) => Home(),
        '/about': (BuildContext context) => About(),
        '/404': (BuildContext context) => NotFound(),
        '/error': (BuildContext context) => ErrorView(),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
