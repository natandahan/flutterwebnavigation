import 'package:flutter/material.dart';

class Topbar extends StatelessWidget {
  const Topbar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('AppBar Demo'),
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.add_alert),
          tooltip: 'Show Snackbar',
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.navigate_next),
          tooltip: 'Next page',
        ),
      ],
    );
  }
}

// /// This is the stateless widget that the main application instantiates.
// class MyStatelessWidget extends StatelessWidget {
//   MyStatelessWidget({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: scaffoldKey,
//       appBar:
//       body: const Center(
//         child: Text(
//           'This is the home page',
//           style: TextStyle(fontSize: 24),
//         ),
//       ),
//     );
//   }
// }
