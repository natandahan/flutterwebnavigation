import 'package:flutter_web_plugins/flutter_web_plugins.dart';

// Web Url Support
void configureApp() {
  setUrlStrategy(PathUrlStrategy());
}
